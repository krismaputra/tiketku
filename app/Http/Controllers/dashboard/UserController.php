<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User; 
use Validator;

class UserController extends Controller
{

   
    //ini user
    public function index(Request $request,User $users){
        //inisialisasi q(query dari halaman users)
        $q = $request->input('q');
        
        //inisialisasi users unutk class active
        $active = 'Users';
        //logika di menu users untuk search
        $users = $users->when($q, function($query) use ($q){
                    return $query->where('name', 'like', '%'.$q.'%')
                                ->orWhere('email','like', '%'.$q.'%'); 
                })
                    ->paginate(10); //untuk mengambil data (jumlah data yang di tampilkan 10 10 gt)

        // dd($users); //cara menentukan kelas active yaitu users, dan mereturnnya ke views
        //memperbaiki pagination dan di taruh di list php
        $request = $request->all();
        return view('dashboard/user/list', ['users' => $users, 'request' => $request, 'active' => $active]);
    }

    public function create(){

    }

    public function show(){

    }

    public function edit($id){

        $user = USER::find($id);
        $active = 'Users';
        return view('dashboard/user/form',['user' => $user, 'active' =>  $active]);
    }

    public function update(Request $request,$id){
        $user = USER::find($id);

        $validator = VALIDATOR::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:App\Models\User,email,'.$id
        ]);
        
        if($validator->fails()){
            return redirect('dashboard/users/'.$id)
            ->withErrors($validator)
            ->withInput();
        }else{
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->save();
            return redirect('dashboard/users');
        }

        
    }

    public function destroy($id){
        $user = USER::find($id);
        $user->delete();
        return redirect('dashboard/users');
    }
}
