<?php

return [

    'store' => 'Penambahan data :title berhasil!.',
    'update' => 'Update data :title berhasil!.',
    'delete' => 'Delete data :title berhasil!.',
    'no_data' => 'Belum ada data di module :module'

];
